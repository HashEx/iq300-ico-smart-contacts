pragma solidity ^0.4.18;

import 'zeppelin-solidity/contracts/token/StandardToken.sol';
import 'zeppelin-solidity/contracts/ownership/Ownable.sol';

contract TempusToken is StandardToken, Ownable {

	string public constant name = "TempusToken";
	string public constant symbol = "TMTT";
	uint8 public constant decimals = 3;

	uint256 public constant INITIAL_SUPPLY = 0;

	event Burn(address indexed from, uint256 value);
	event Mint(address indexed receiver, uint256 value);

	mapping(address => bool) burners;
	mapping(address => bool) minters;

	modifier onlyMinters() {
		require(minters[msg.sender]);
		_;
	}

	modifier onlyBurners() {
		require(burners[msg.sender]);
		_;
	}

	/**
	* @dev Token constructor that sets intial values and gives initial supply to msg.sender
	*/
	function TempusToken() public {
		totalSupply = INITIAL_SUPPLY;
		balances[msg.sender] = INITIAL_SUPPLY;
		minters[msg.sender] = true;
		burners[msg.sender] = true;
	}

	/**
	* @dev Function to mint tokens
	* @param receiver The address that will receive the minted tokens
	* @param amount The amount of tokens to mint
	* @return A boolean that indicates if operation was successful
	*/
	function mint(address receiver, uint256 amount) public onlyMinters returns (bool success) {
		balances[receiver] = balances[receiver].add(amount);
		totalSupply = totalSupply.add(amount);
		Mint(receiver, amount);
		return true;
	}

	/**
	* @dev Function to burn tokens
	* @param from The address that tokens will be burned from
	* @param value The amount of tokens to burn
	* @return A boolean that indicates if operation was successful
	*/
	function burn(address from, uint256 value) public onlyBurners returns (bool success) {
		require(balances[from] >= value);

		balances[from] = balances[from].sub(value);
		totalSupply = totalSupply.sub(value);
		Burn(from, value);
		return true;
	}

	/**
	* @dev Function to set addresses that will be able to mint tokens
	* @param addr The address that will be set as minter or not
	* @param isMinter A boolean that indicates whether address should be set as minter
	*/
	function setAsMinter(address addr, bool isMinter) public onlyOwner {
		minters[addr] = isMinter;
	}

	/**
	* @dev Function to set addresses that will be able to burn tokens
	* @param addr The address that will be set as burner or not
	* @param isBurner A boolean that indicates whether address should be set as burner
	*/
	function setAsBurner(address addr, bool isBurner) public onlyOwner {
		burners[addr] = isBurner;
	}

}