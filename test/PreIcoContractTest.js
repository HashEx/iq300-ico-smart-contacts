const TempusToken = artifacts.require('./TempusToken')
const TempusPreIco = artifacts.require('./TempusPreIco')

import moment from 'moment'
import ether from './helpers/ether'
import latestTime from './helpers/latestTime'
import increaseTime from './helpers/increaseTime'

contract('TempusPreIco', accounts => {

	const owner = accounts[0]
	const tokenPrice = ether(0.005)

	beforeEach(async function() {
		this.startTime = latestTime().add(1, 'day').unix()
		this.endTime = latestTime().add(3, 'day').unix()
		this.token = await TempusToken.new()
		this.withdrawAddress1 = accounts[5];
		this.withdrawAddress2 = accounts[6];
		this.preico = await TempusPreIco.new(this.token.address,
			'' + this.withdrawAddress1, '' + this.withdrawAddress2)
		await this.preico.setStartTime(this.startTime)
		await this.preico.setEndTime(this.endTime)
		await this.token.setAsMinter(this.preico.address, true)
	})

	it('should set correct initial values', async function() {
		const hardcap = await this.preico.hardCap.call()
		assert.equal(hardcap.valueOf(), 860000000, "hardcap should be set")

		const tokensSold = await this.preico.tokensSold.call()
		assert.equal(tokensSold.valueOf(), 0, "tokensSold should have initial value 0")
	})

	it('should set start time', async function() {
		const newStartTime = 1000;

		await this.preico.setStartTime(newStartTime)

		const savedStartTime = await this.preico.startTime.call()
		assert.equal(savedStartTime.valueOf(), 1000, "start time should be set")

		await this.preico.setStartTime(2000, {from: accounts[1]}).should.be.rejected
		const notUpdatedStartTime = await this.preico.startTime.call()
		assert.equal(notUpdatedStartTime.valueOf(), savedStartTime.valueOf(),
			"start time should not be updated by not owner")
	})

	it('should set end time', async function() {
		const newEndTime = 1000;

		await this.preico.setEndTime(newEndTime)

		const savedEndTime = await this.preico.endTime.call()
		assert.equal(savedEndTime.valueOf(), 1000, "end time should be set")

		await this.preico.setEndTime(2000, {from: accounts[1]}).should.be.rejected
		const notUpdatedEndTime = await this.preico.endTime.call()
		assert.equal(notUpdatedEndTime.valueOf(), savedEndTime.valueOf(),
			"end time should not be updated by not owner")
	})

	it('should not allow to make a purchase on not active ico', async function() {
		const isActive = await this.preico.isActive.call()
		assert.equal(isActive, false, "precoditions: ico should not be active")
		const beneficiary = accounts[1]
		await this.preico.sendTransaction({from:beneficiary, value: tokenPrice}).should.be.rejected
	})

	describe('test active pre ico: ', async function() {

		beforeEach(async function() {
			await increaseTime(moment.duration(2, 'day'))
		})

		it('should buy tokens on sending ether to pre ico contract', async function() {
			const beneficiary = accounts[1]
			const initialBeneficaryBalance = await this.token.balanceOf.call(beneficiary)
			assert.equal(initialBeneficaryBalance.valueOf(), 0, "initial balance should be zero")

			await this.preico.sendTransaction({from:beneficiary, value: tokenPrice * 100})

			const balance = await this.token.balanceOf.call(beneficiary)
			assert.equal(balance.valueOf(), 100*1000, "tokens should be bought")
		})

		it('should not allow to make purchase for 0x0 address', async function() {
			const beneficiary = accounts[1]

			await this.preico.sendTransaction({from:0x0, value: tokenPrice * 100}).should.be.rejected
		})

		it('should not allow to purchase less tokens than minimum amount', async function() {
			const minimumAmountInWei = ether(0.005 * 19.999);
			await this.preico.sendTransaction({value: (minimumAmountInWei)}).should.be.rejected
			//minimum amount should not be rejected
			await this.preico.sendTransaction({value: ether(0.005*20)})
		})

		it('should allow to buy less than minimum threshould to close pre ico', async function() {
			const hardcap = await this.preico.hardCap.call()
			let isActive = await this.preico.isActive.call()
			assert.equal(isActive, true, "pre ico should be active")

			const robot = accounts[2]
			await this.preico.setAsSeller(robot, true)
			await this.preico.externalPurchase(robot, hardcap - 1000, {from: robot})

			await this.preico.sendTransaction({value: ether(0.005)}) //should not throw
		})

		it('should buy tokens with buyFor function', async function() {
			const beneficiary = accounts[1]
			const initialBeneficaryBalance = await this.token.balanceOf.call(beneficiary)
			assert.equal(initialBeneficaryBalance.valueOf(), 0, "initial balance should be zero")

			await this.preico.buyFor(beneficiary, {from: beneficiary, value: tokenPrice * 100})

			const balance = await this.token.balanceOf.call(beneficiary)
			assert.equal(balance.valueOf(), 100 * 1000, "tokens should be bought")
		})

		it('should throw on buyFor function with 0 ether', async function() {
			const beneficiary = accounts[1]
			await this.preico.buyFor(beneficiary).should.be.rejected
		})

		it('should pause ico', async function() {
			await this.preico.setPaused(true);
			let active = await this.preico.isActive.call();
			assert.equal(active, false, "ico should be paused")

			await this.preico.setPaused(false);
			active = await this.preico.isActive.call();
			assert.equal(active, true, "ico should be paused")
		})

		it('should not allow not owner to pause ico', async function() {
			await this.preico.setPaused(true, {from: accounts[1]}).should.be.rejected
		})

		it('should buy tokens with external purchase function', async function() {
			const beneficiary = accounts[1]
			const initialBeneficaryBalance = await this.token.balanceOf.call(beneficiary)
			assert.equal(initialBeneficaryBalance.valueOf(), 0, "initial balance should be zero")

			const robot = accounts[2]
			await this.preico.setAsSeller(robot, true)

			await this.preico.externalPurchase(beneficiary, 100*1000, {from: robot})
			const balance = await this.token.balanceOf.call(beneficiary)
			assert.equal(balance.valueOf(), 100*1000, "tokens should be bought")
		})

		it('should not allow to make external purchasel for not sellers', async function() {
			await this.preico.setAsSeller(accounts[1], true)
			await this.preico.externalPurchase(accounts[2], 100*1000, {from: accounts[2]}).should.be.rejected
		})

		it('should not let exceed hardcap', async function() {
			const hardcap = await this.preico.hardCap.call()
			let isActive = await this.preico.isActive.call()
			assert.equal(isActive, true, "pre ico should be active")

			const robot = accounts[2]
			await this.preico.setAsSeller(robot, true)

			const exceedAmount = hardcap.add(1)

			await this.preico.externalPurchase(robot, exceedAmount, {from: robot}).should.be.rejected
		})

		it('should end sale when all tokens are sold', async function() {
			const hardcap = await this.preico.hardCap.call()
			let isActive = await this.preico.isActive.call()
			assert.equal(isActive, true, "pre ico should be active")

			const robot = accounts[2]
			await this.preico.setAsSeller(robot, true)
			await this.preico.externalPurchase(robot, hardcap, {from: robot})

			const tokensSold = await this.preico.tokensSold.call()
			assert.equal(hardcap.valueOf(), tokensSold.valueOf(), "tokens sold value should be updated")

			isActive = await this.preico.isActive.call()
			assert.equal(isActive, false, "pre ico should be finished")
		})

		it('should withdraw ether', async function() {
			const initialBalance1 = web3.eth.getBalance(this.withdrawAddress1)
			const initialBalance2 = web3.eth.getBalance(this.withdrawAddress2)

			const beneficiary = accounts[1]
			await this.preico.sendTransaction({from:beneficiary, value: tokenPrice * 100})

			await this.preico.withdrawEther(tokenPrice * 100)
			const balanceAfterWithdrawal1 = web3.eth.getBalance(this.withdrawAddress1)
			assert.equal(balanceAfterWithdrawal1, initialBalance1.add(tokenPrice * 50).valueOf(),
				"ether should be withdrawn")
			const balanceAfterWithdrawal2 = web3.eth.getBalance(this.withdrawAddress2)
			assert.equal(balanceAfterWithdrawal2, initialBalance1.add(tokenPrice * 50).valueOf(),
				"ether should be withdrawn")
		})


	})


})