const TempusToken = artifacts.require('./TempusToken')

import EVMThrow from './helpers/EVMThrow'
const BigNumber = web3.BigNumber
const should = require('chai')
  .use(require('chai-as-promised'))
  .use(require('chai-bignumber')(BigNumber))
  .should()

contract('TempusToken', accounts => {

	beforeEach(async function() {
		this.owner = accounts[0]
		this.token = await TempusToken.new()
		await this.token.mint(this.owner, 1000000)
	})

	it('total supply should be set', async function() {
		const totalSupply = await this.token.totalSupply();
		assert.equal(1000000, totalSupply.valueOf(), "correct total supply should be set")
	})

	it('should be minted only by admin', async function() {
		const admin = accounts[1]
		await this.token.mint(admin, 1000, {from: admin}).should.be.rejected

		await this.token.setAsMinter(admin, true)
		await this.token.mint(admin, 1000, {from: admin})
		
		const balance = await this.token.balanceOf.call(admin)
		assert.equal(balance.valueOf(), 1000, "tokens should be minted to specified address")
	})

	it('should be burned only by admin', async function() {
		const admin = accounts[1]
		await this.token.transfer(admin, 1000)
		const balance = await this.token.balanceOf.call(admin)  
		assert.equal(balance.valueOf(), 1000, "tokens should be trafserred to specified address")

		await this.token.burn(admin, 1000, {from: admin}).should.be.rejected

		await this.token.setAsBurner(admin, true)
		await this.token.burn(admin, 1000, {from: admin})

		const newBalance = await this.token.balanceOf(admin)
	})

	it('should change total supply after mint and burn', async function() {
		let totalTokens = await this.token.totalSupply()
		await this.token.mint(this.owner, 1000)
		let newTotalTokens = await this.token.totalSupply()
		assert.equal(newTotalTokens.valueOf(), totalTokens.add(1000), 
			"mint should properly increase token total supply")

		await this.token.burn(this.owner, 2000)
		const tokensAfterBurn = await this.token.totalSupply.call();
		assert.equal(tokensAfterBurn.valueOf(), newTotalTokens.sub(2000), 
			"total supply should be decreased after burn ")
	})

	it('should throw on burning tokens more than balance', async function() {
		const anotherAccount = accounts[1]
		await this.token.transfer(anotherAccount, 1000)

		await this.token.burn(anotherAccount, 1001).should.be.rejected
	})

	it('should not allow burners to mint', async function() {
		const anotherAccount = accounts[1]
		await this.token.setAsBurner(anotherAccount, true)
		await this.token.mint(this.owner, 100, {from: anotherAccount}).should.be.rejected
	})

	it('should not allow minters to burn', async function() {
		const anotherAccount = accounts[1]
		await this.token.setAsMinter(anotherAccount, true)
		await this.token.burn(this.owner, 100, {from: anotherAccount}).should.be.rejected
	})

})